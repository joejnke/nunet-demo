# Nunet Demo App

The app is live at http://demo.nunet.io. Current status: [v0.1 alpha-release](https://gitlab.com/nunet/nunet-demo/-/releases#v0.1).

NuNet allows to run SingularityNET services on computing resources that are pooled from independent providers and connect several of them to perform a more complex task or workflow. Hardware device providers that participate in the ecosystem are compensated with NuNet tokens. 

This demo app showcases an orchestration of distributed execution of an AI task on separate hardware components and adequately reimbursing them for used computing resources and time. The AI workflow consists of object recognition and dog breed recognition services offered at [SingularityNET](https://singularitynet.io) marketplace.

Each new user of the demo app is provided an initial amount of tokens and can initiate AI task execution by submitting a dog picture and the amount of required tokens to cover computing resources. If a dog and its breed is recognized in the submitted image, a user is awarded an additional amount of tokens (depending on the breed of the dog discovered). A user can execute as many tasks as his/her token balance allows.

* [Release notes](https://gitlab.com/nunet/nunet-demo/-/releases#v0.1)
* [Submitting bug reports and feature requests](https://gitlab.com/nunet/nunet-demo/wikis/Submitting%20bug%20reports%20and%20feature%20requests)
* [Developer flow description](https://gitlab.com/nunet/nunet-demo/wikis/Development%20flow)

![](webapp/public/architecture/nunet_demo_architecture.svg)


The full NuNet version will enable computational economy consisting of any processes, hardware devices and computing frameworks. See https://nunet.io for more.



