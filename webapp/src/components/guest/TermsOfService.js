import React, {useState, useEffect} from 'react';
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TermsofServiceIcon from "../../icons/IconsNunet10";
import Divider from "@material-ui/core/Divider";
import Link from "@material-ui/core/Link";
import { Dialog} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    gutterTop: {
        paddingTop: theme.spacing(2)
    },
    innerContainer: {
        padding: 0
    },
    half_divider: {
        width: "40%",
        margin: "0 auto"
    },
    under_nunet_icon: {
        marginTop: theme.spacing(-3)
    },
    nunet_icon: {
        height: "8rem",
        marginTop: "0rem"
    }
}));

export default function TermsOfService(props){
    const classes = useStyles();

    return(
        <React.Fragment>
            <TermsofServiceIcon className={classes.nunet_icon} />
            <Typography
                variant="h6"
                gutterBottom
                className={classes.under_nunet_icon}    
            >
                Terms of Service
            </Typography>

            <Divider></Divider>

            <Typography
                variant="body1"
                className={classes.gutterTop}
                gutterBottom
                align="justify"
            >
                The application is provided 'AS IS' for demonstration purposes only, 
                without warranty of any kind, express or implied, 
                including but not limited to the warranties of merchantability, 
                fitness for a particular purpose and non-infringement. 
                In no event shall the authors or copyright holders be liable for any claim, damages or other liability, 
                whether in an action of contract, tort of otherwise, arising from, out of or in connection with the applications 
                and its software or the use or other dealings in the software.
            </Typography>    
                <Divider></Divider>
            <Typography
                variant="body1"
                className={classes.gutterTop}
                gutterBottom
                align="justify"
            >

                NTXd tokens are designated for the demonstration purposes within the scope of this application only. 
                NTXd DO NOT represent actually implemented crypto-currency tokens and WILL NOT be implemented in the future. 
                The actual utility tokens of NuNet platform will be implemented as per development strategy independently of this demo application.
                The application may collect information about users. 
                Information collected and cases of use when it is done so are described in 
                <Link 
                    href="https://gitlab.com/nunet/nunet-demo/master/link" 
                    underline="hover" 
                    target="_blank"> privacy policy </Link> 
                document. 
                Accepting these terms of service implies that a user has read and accepted the privacy policy. 
                The nature of information collected by the application depends on the chosen login method, 
                where 'guest access' collects and stores the least personal information.

            </Typography>

            <Dialog>Agree</Dialog>
        </React.Fragment>
    );
}