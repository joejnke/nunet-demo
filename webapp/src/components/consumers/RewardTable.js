import React, { useState, useEffect, useContext } from "react";

import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";

import RewardIcon from "mdi-material-ui/GiftOutline";
import ReplayIcon from "@material-ui/icons/Replay";
import DogIcon from "@material-ui/icons/Pets";
import CoinIcon from "mdi-material-ui/Coins";

import { call_grpc } from "../utils/grpc";
import { base64_src } from "../utils/images";
import IconTypography from "../commons/IconTypography";
import Loading from "../commons/Loading";
import CardItem from "../commons/CardItem";
import { CacheContext, SnackBarContext } from "../utils/contexts";
import ShowInfo from "../commons/ShowInfo";
import StartTaskButton from "../commons/StartTask";

const { PageInput } = require("../../grpc/session_pb");

export default function RewardTable(props) {
    const itemsPerPage = 5;

    const showSnackBar = useContext(SnackBarContext);
    const [cache, updateCache] = useContext(CacheContext);

    const [loading, setLoading] = useState(false);

    const [rewards, setRewards] = useState(cache.rewards || null);
    const [noMoreItems, setNoMoreItems] = useState(false);

    function on_grpc_error(err) {
        setLoading(false);
    }

    function parseGrpcReward(reward) {
        return {
            base64: reward.getBase64(),
            breed_name: reward.getBreedName(),
            reward: reward.getReward()
        };
    }

    function clean_breed_name(breed_name) {
        return breed_name.replace("_", " ");
    }

    function on_rewards_response(response) {
        setLoading(false);
        if (response.getRewardsList().length > 0) {
            let parsed_rewards = response.getRewardsList().map(parseGrpcReward);
            setRewards(rewards =>
                rewards ? [...rewards, ...parsed_rewards] : parsed_rewards
            );
        } else if (rewards === null) {
            // no rewards found should be empty array, but rewards stay null if error happens
            setRewards([]);
        } else {
            // this means all rewards are already loaded, nothing to load anymore
            setNoMoreItems(true);
        }
    }

    function get_rewards() {
        setLoading(true);
        let request = new PageInput();
        request.setOffset(rewards ? rewards.length : 0);
        request.setSize(itemsPerPage);

        call_grpc(
            "rewardTable",
            request,
            on_rewards_response,
            showSnackBar,
            on_grpc_error
        );
    }

    useEffect(() => {
        if (!rewards) {
            get_rewards();
        }

        return () => {
            updateCache({ rewards: rewards });
        };
    }, [rewards]);

    return (
        <React.Fragment>
            <IconTypography variant="h5" gutterBottom icon={RewardIcon}>
                Reward Table
            </IconTypography>

            {rewards &&
                Object.keys(rewards).map(index => (
                    <CardItem
                        key={index}
                        image={base64_src(rewards[index].base64)}
                        title={clean_breed_name(rewards[index].breed_name)}
                        titleIcon={DogIcon}
                        subtitle={rewards[index].reward + "NTXd"}
                        subtitleIcon={CoinIcon}
                    />
                ))}

            {/* show loading indicator */}
            {loading && <Loading />}

            {!loading &&
                rewards &&
                rewards.length !== 0 &&
                rewards.length % itemsPerPage === 0 &&
                !noMoreItems && (
                    <Box m={2}>
                        <Button
                            startIcon={<ReplayIcon />}
                            variant="contained"
                            color="primary"
                            onClick={get_rewards}
                        >
                            Load more
                        </Button>
                    </Box>
                )}

            {!loading && rewards && rewards.length === 0 && (
                <React.Fragment>
                    <Box mt={3}>
                        <ShowInfo>
                            There are no rewards data available for now!
                        </ShowInfo>
                        <StartTaskButton history={props.history} />
                    </Box>
                </React.Fragment>
            )}
        </React.Fragment>
    );
}
