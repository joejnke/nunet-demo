/* eslint-disable */
/**
 * @fileoverview gRPC-Web generated client stub for session_manager
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js')
const proto = {};
proto.session_manager = require('./session_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.session_manager.SessionManagerClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.session_manager.SessionManagerPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.session_manager.SignupInput,
 *   !proto.session_manager.SignupOutput>}
 */
const methodDescriptor_SessionManager_signup = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/signup',
  grpc.web.MethodType.UNARY,
  proto.session_manager.SignupInput,
  proto.session_manager.SignupOutput,
  /**
   * @param {!proto.session_manager.SignupInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.SignupOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.session_manager.SignupInput,
 *   !proto.session_manager.SignupOutput>}
 */
const methodInfo_SessionManager_signup = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.SignupOutput,
  /**
   * @param {!proto.session_manager.SignupInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.SignupOutput.deserializeBinary
);


/**
 * @param {!proto.session_manager.SignupInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.SignupOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.SignupOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.signup =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/signup',
      request,
      metadata || {},
      methodDescriptor_SessionManager_signup,
      callback);
};


/**
 * @param {!proto.session_manager.SignupInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.SignupOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.signup =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/signup',
      request,
      metadata || {},
      methodDescriptor_SessionManager_signup);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.session_manager.LoginInput,
 *   !proto.session_manager.LoginOutput>}
 */
const methodDescriptor_SessionManager_login = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/login',
  grpc.web.MethodType.UNARY,
  proto.session_manager.LoginInput,
  proto.session_manager.LoginOutput,
  /**
   * @param {!proto.session_manager.LoginInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.LoginOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.session_manager.LoginInput,
 *   !proto.session_manager.LoginOutput>}
 */
const methodInfo_SessionManager_login = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.LoginOutput,
  /**
   * @param {!proto.session_manager.LoginInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.LoginOutput.deserializeBinary
);


/**
 * @param {!proto.session_manager.LoginInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.LoginOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.LoginOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.login =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/login',
      request,
      metadata || {},
      methodDescriptor_SessionManager_login,
      callback);
};


/**
 * @param {!proto.session_manager.LoginInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.LoginOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.login =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/login',
      request,
      metadata || {},
      methodDescriptor_SessionManager_login);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.session_manager.LoginInput,
 *   !proto.session_manager.LoginOutput>}
 */
const methodDescriptor_SessionManager_guestLogin = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/guestLogin',
  grpc.web.MethodType.UNARY,
  proto.session_manager.LoginInput,
  proto.session_manager.LoginOutput,
  /**
   * @param {!proto.session_manager.LoginInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.LoginOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.session_manager.LoginInput,
 *   !proto.session_manager.LoginOutput>}
 */
const methodInfo_SessionManager_guestLogin = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.LoginOutput,
  /**
   * @param {!proto.session_manager.LoginInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.LoginOutput.deserializeBinary
);


/**
 * @param {!proto.session_manager.LoginInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.LoginOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.LoginOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.guestLogin =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/guestLogin',
      request,
      metadata || {},
      methodDescriptor_SessionManager_guestLogin,
      callback);
};


/**
 * @param {!proto.session_manager.LoginInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.LoginOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.guestLogin =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/guestLogin',
      request,
      metadata || {},
      methodDescriptor_SessionManager_guestLogin);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.session_manager.SmediaLoginInput,
 *   !proto.session_manager.LoginOutput>}
 */
const methodDescriptor_SessionManager_socialMediaLogin = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/socialMediaLogin',
  grpc.web.MethodType.UNARY,
  proto.session_manager.SmediaLoginInput,
  proto.session_manager.LoginOutput,
  /**
   * @param {!proto.session_manager.SmediaLoginInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.LoginOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.session_manager.SmediaLoginInput,
 *   !proto.session_manager.LoginOutput>}
 */
const methodInfo_SessionManager_socialMediaLogin = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.LoginOutput,
  /**
   * @param {!proto.session_manager.SmediaLoginInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.LoginOutput.deserializeBinary
);


/**
 * @param {!proto.session_manager.SmediaLoginInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.LoginOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.LoginOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.socialMediaLogin =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/socialMediaLogin',
      request,
      metadata || {},
      methodDescriptor_SessionManager_socialMediaLogin,
      callback);
};


/**
 * @param {!proto.session_manager.SmediaLoginInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.LoginOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.socialMediaLogin =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/socialMediaLogin',
      request,
      metadata || {},
      methodDescriptor_SessionManager_socialMediaLogin);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.session_manager.LogoutInput,
 *   !proto.session_manager.LogoutOutput>}
 */
const methodDescriptor_SessionManager_logout = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/logout',
  grpc.web.MethodType.UNARY,
  proto.session_manager.LogoutInput,
  proto.session_manager.LogoutOutput,
  /**
   * @param {!proto.session_manager.LogoutInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.LogoutOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.session_manager.LogoutInput,
 *   !proto.session_manager.LogoutOutput>}
 */
const methodInfo_SessionManager_logout = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.LogoutOutput,
  /**
   * @param {!proto.session_manager.LogoutInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.LogoutOutput.deserializeBinary
);


/**
 * @param {!proto.session_manager.LogoutInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.LogoutOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.LogoutOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.logout =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/logout',
      request,
      metadata || {},
      methodDescriptor_SessionManager_logout,
      callback);
};


/**
 * @param {!proto.session_manager.LogoutInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.LogoutOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.logout =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/logout',
      request,
      metadata || {},
      methodDescriptor_SessionManager_logout);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.session_manager.ExecutionInput,
 *   !proto.session_manager.ExecutionOutput>}
 */
const methodDescriptor_SessionManager_execute = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/execute',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.session_manager.ExecutionInput,
  proto.session_manager.ExecutionOutput,
  /**
   * @param {!proto.session_manager.ExecutionInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.ExecutionOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.session_manager.ExecutionInput,
 *   !proto.session_manager.ExecutionOutput>}
 */
const methodInfo_SessionManager_execute = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.ExecutionOutput,
  /**
   * @param {!proto.session_manager.ExecutionInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.ExecutionOutput.deserializeBinary
);


/**
 * @param {!proto.session_manager.ExecutionInput} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.ExecutionOutput>}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.execute =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/session_manager.SessionManager/execute',
      request,
      metadata || {},
      methodDescriptor_SessionManager_execute);
};


/**
 * @param {!proto.session_manager.ExecutionInput} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.ExecutionOutput>}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerPromiseClient.prototype.execute =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/session_manager.SessionManager/execute',
      request,
      metadata || {},
      methodDescriptor_SessionManager_execute);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.session_manager.UserInfoOutput>}
 */
const methodDescriptor_SessionManager_userInfo = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/userInfo',
  grpc.web.MethodType.UNARY,
  google_protobuf_empty_pb.Empty,
  proto.session_manager.UserInfoOutput,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.UserInfoOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.session_manager.UserInfoOutput>}
 */
const methodInfo_SessionManager_userInfo = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.UserInfoOutput,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.UserInfoOutput.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.UserInfoOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.UserInfoOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.userInfo =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/userInfo',
      request,
      metadata || {},
      methodDescriptor_SessionManager_userInfo,
      callback);
};


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.UserInfoOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.userInfo =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/userInfo',
      request,
      metadata || {},
      methodDescriptor_SessionManager_userInfo);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.session_manager.ProcessInfoInput,
 *   !proto.session_manager.ProcessInfoOutput>}
 */
const methodDescriptor_SessionManager_processInfo = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/processInfo',
  grpc.web.MethodType.UNARY,
  proto.session_manager.ProcessInfoInput,
  proto.session_manager.ProcessInfoOutput,
  /**
   * @param {!proto.session_manager.ProcessInfoInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.ProcessInfoOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.session_manager.ProcessInfoInput,
 *   !proto.session_manager.ProcessInfoOutput>}
 */
const methodInfo_SessionManager_processInfo = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.ProcessInfoOutput,
  /**
   * @param {!proto.session_manager.ProcessInfoInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.ProcessInfoOutput.deserializeBinary
);


/**
 * @param {!proto.session_manager.ProcessInfoInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.ProcessInfoOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.ProcessInfoOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.processInfo =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/processInfo',
      request,
      metadata || {},
      methodDescriptor_SessionManager_processInfo,
      callback);
};


/**
 * @param {!proto.session_manager.ProcessInfoInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.ProcessInfoOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.processInfo =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/processInfo',
      request,
      metadata || {},
      methodDescriptor_SessionManager_processInfo);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.session_manager.ProcessInfoInput,
 *   !proto.session_manager.ShareTaskOutput>}
 */
const methodDescriptor_SessionManager_share = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/share',
  grpc.web.MethodType.UNARY,
  proto.session_manager.ProcessInfoInput,
  proto.session_manager.ShareTaskOutput,
  /**
   * @param {!proto.session_manager.ProcessInfoInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.ShareTaskOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.session_manager.ProcessInfoInput,
 *   !proto.session_manager.ShareTaskOutput>}
 */
const methodInfo_SessionManager_share = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.ShareTaskOutput,
  /**
   * @param {!proto.session_manager.ProcessInfoInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.ShareTaskOutput.deserializeBinary
);


/**
 * @param {!proto.session_manager.ProcessInfoInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.ShareTaskOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.ShareTaskOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.share =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/share',
      request,
      metadata || {},
      methodDescriptor_SessionManager_share,
      callback);
};


/**
 * @param {!proto.session_manager.ProcessInfoInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.ShareTaskOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.share =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/share',
      request,
      metadata || {},
      methodDescriptor_SessionManager_share);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.session_manager.PageInput,
 *   !proto.session_manager.RewardTableOutput>}
 */
const methodDescriptor_SessionManager_rewardTable = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/rewardTable',
  grpc.web.MethodType.UNARY,
  proto.session_manager.PageInput,
  proto.session_manager.RewardTableOutput,
  /**
   * @param {!proto.session_manager.PageInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.RewardTableOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.session_manager.PageInput,
 *   !proto.session_manager.RewardTableOutput>}
 */
const methodInfo_SessionManager_rewardTable = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.RewardTableOutput,
  /**
   * @param {!proto.session_manager.PageInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.RewardTableOutput.deserializeBinary
);


/**
 * @param {!proto.session_manager.PageInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.RewardTableOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.RewardTableOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.rewardTable =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/rewardTable',
      request,
      metadata || {},
      methodDescriptor_SessionManager_rewardTable,
      callback);
};


/**
 * @param {!proto.session_manager.PageInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.RewardTableOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.rewardTable =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/rewardTable',
      request,
      metadata || {},
      methodDescriptor_SessionManager_rewardTable);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.session_manager.PageInput,
 *   !proto.session_manager.PreviousTasksOutput>}
 */
const methodDescriptor_SessionManager_previousTasks = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/previousTasks',
  grpc.web.MethodType.UNARY,
  proto.session_manager.PageInput,
  proto.session_manager.PreviousTasksOutput,
  /**
   * @param {!proto.session_manager.PageInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.PreviousTasksOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.session_manager.PageInput,
 *   !proto.session_manager.PreviousTasksOutput>}
 */
const methodInfo_SessionManager_previousTasks = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.PreviousTasksOutput,
  /**
   * @param {!proto.session_manager.PageInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.PreviousTasksOutput.deserializeBinary
);


/**
 * @param {!proto.session_manager.PageInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.PreviousTasksOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.PreviousTasksOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.previousTasks =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/previousTasks',
      request,
      metadata || {},
      methodDescriptor_SessionManager_previousTasks,
      callback);
};


/**
 * @param {!proto.session_manager.PageInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.PreviousTasksOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.previousTasks =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/previousTasks',
      request,
      metadata || {},
      methodDescriptor_SessionManager_previousTasks);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.session_manager.ProviderOutput>}
 */
const methodDescriptor_SessionManager_provider = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/provider',
  grpc.web.MethodType.UNARY,
  google_protobuf_empty_pb.Empty,
  proto.session_manager.ProviderOutput,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.ProviderOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.session_manager.ProviderOutput>}
 */
const methodInfo_SessionManager_provider = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.ProviderOutput,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.ProviderOutput.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.ProviderOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.ProviderOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.provider =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/provider',
      request,
      metadata || {},
      methodDescriptor_SessionManager_provider,
      callback);
};


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.ProviderOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.provider =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/provider',
      request,
      metadata || {},
      methodDescriptor_SessionManager_provider);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.session_manager.ProviderDeviceInput,
 *   !proto.session_manager.ProviderDeviceOutput>}
 */
const methodDescriptor_SessionManager_providerDevice = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/providerDevice',
  grpc.web.MethodType.UNARY,
  proto.session_manager.ProviderDeviceInput,
  proto.session_manager.ProviderDeviceOutput,
  /**
   * @param {!proto.session_manager.ProviderDeviceInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.ProviderDeviceOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.session_manager.ProviderDeviceInput,
 *   !proto.session_manager.ProviderDeviceOutput>}
 */
const methodInfo_SessionManager_providerDevice = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.ProviderDeviceOutput,
  /**
   * @param {!proto.session_manager.ProviderDeviceInput} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.ProviderDeviceOutput.deserializeBinary
);


/**
 * @param {!proto.session_manager.ProviderDeviceInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.ProviderDeviceOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.ProviderDeviceOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.providerDevice =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/providerDevice',
      request,
      metadata || {},
      methodDescriptor_SessionManager_providerDevice,
      callback);
};


/**
 * @param {!proto.session_manager.ProviderDeviceInput} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.ProviderDeviceOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.providerDevice =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/providerDevice',
      request,
      metadata || {},
      methodDescriptor_SessionManager_providerDevice);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.session_manager.UpdateTokenOutput>}
 */
const methodDescriptor_SessionManager_updateUserToken = new grpc.web.MethodDescriptor(
  '/session_manager.SessionManager/updateUserToken',
  grpc.web.MethodType.UNARY,
  google_protobuf_empty_pb.Empty,
  proto.session_manager.UpdateTokenOutput,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.UpdateTokenOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.session_manager.UpdateTokenOutput>}
 */
const methodInfo_SessionManager_updateUserToken = new grpc.web.AbstractClientBase.MethodInfo(
  proto.session_manager.UpdateTokenOutput,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.session_manager.UpdateTokenOutput.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.session_manager.UpdateTokenOutput)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.session_manager.UpdateTokenOutput>|undefined}
 *     The XHR Node Readable Stream
 */
proto.session_manager.SessionManagerClient.prototype.updateUserToken =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/session_manager.SessionManager/updateUserToken',
      request,
      metadata || {},
      methodDescriptor_SessionManager_updateUserToken,
      callback);
};


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.session_manager.UpdateTokenOutput>}
 *     A native promise that resolves to the response
 */
proto.session_manager.SessionManagerPromiseClient.prototype.updateUserToken =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/session_manager.SessionManager/updateUserToken',
      request,
      metadata || {},
      methodDescriptor_SessionManager_updateUserToken);
};


module.exports = proto.session_manager;

