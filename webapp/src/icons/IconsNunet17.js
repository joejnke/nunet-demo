import React from "react";

const SvgIconsnunet17 = props => (
  <svg data-name="Layer 1" viewBox="0 0 600 600" {...props}>
    <defs>
      <linearGradient
        id="icons_nunet-17_svg__a"
        x1={124.61}
        y1={472.39}
        x2={465.82}
        y2={490.45}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.53} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-17_svg__b"
        x1={300.2}
        y1={273.84}
        x2={314.18}
        y2={442.92}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#3eb3e6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-17_svg__c"
        x1={127.8}
        y1={394.06}
        x2={469.01}
        y2={412.12}
        xlinkHref="#icons_nunet-17_svg__a"
      />
      <linearGradient
        id="icons_nunet-17_svg__d"
        x1={300.2}
        y1={195.34}
        x2={314.18}
        y2={364.42}
        xlinkHref="#icons_nunet-17_svg__b"
      />
      <linearGradient
        id="icons_nunet-17_svg__e"
        x1={131.08}
        y1={313.59}
        x2={472.29}
        y2={331.66}
        xlinkHref="#icons_nunet-17_svg__a"
      />
      <linearGradient
        id="icons_nunet-17_svg__f"
        x1={300.2}
        y1={114.7}
        x2={314.18}
        y2={283.78}
        xlinkHref="#icons_nunet-17_svg__b"
      />
      <linearGradient
        id="icons_nunet-17_svg__g"
        x1={134.36}
        y1={233.13}
        x2={475.56}
        y2={251.19}
        xlinkHref="#icons_nunet-17_svg__a"
      />
      <linearGradient
        id="icons_nunet-17_svg__h"
        x1={349.16}
        y1={154.74}
        x2={256.43}
        y2={245.2}
        xlinkHref="#icons_nunet-17_svg__b"
      />
      <linearGradient
        id="icons_nunet-17_svg__i"
        x1={228.66}
        y1={298.06}
        x2={227.16}
        y2={753.23}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
    </defs>
    <path
      d="M149.05 429.71s.38 68.87 1.09 70.69c9.41 23.92 78.77 42.5 162.94 42.5 85 0 154.94-19 163.21-43.23l.82-70z"
      fill="url(#icons_nunet-17_svg__a)"
    />
    <ellipse
      cx={313.09}
      cy={429.71}
      rx={164.03}
      ry={48.06}
      fill="url(#icons_nunet-17_svg__b)"
    />
    <path
      d="M149.05 351.21s.38 68.87 1.09 70.69c9.41 23.92 78.77 42.49 162.94 42.49 85 0 154.94-18.95 163.21-43.22l.82-70z"
      fill="url(#icons_nunet-17_svg__c)"
    />
    <ellipse
      cx={313.09}
      cy={351.21}
      rx={164.03}
      ry={48.06}
      fill="url(#icons_nunet-17_svg__d)"
    />
    <path
      d="M149.05 270.57s.38 68.87 1.09 70.69c9.41 23.92 78.77 42.5 162.94 42.5 85 0 154.94-19 163.21-43.23l.82-70z"
      fill="url(#icons_nunet-17_svg__e)"
    />
    <ellipse
      cx={313.09}
      cy={270.57}
      rx={164.03}
      ry={48.06}
      fill="url(#icons_nunet-17_svg__f)"
    />
    <path
      d="M149.05 189.93s.38 68.87 1.09 70.69c9.41 23.92 78.77 42.5 162.94 42.5 85 0 154.94-19 163.21-43.23l.82-70z"
      fill="url(#icons_nunet-17_svg__g)"
    />
    <ellipse
      cx={313.09}
      cy={189.93}
      rx={164.03}
      ry={48.06}
      fill="url(#icons_nunet-17_svg__h)"
    />
    <path
      d="M309.62 386.69h-28.1v-42.91a15.51 15.51 0 00-15.64-15.35h-75.5a15.51 15.51 0 00-15.64 15.35v42.91h-28.1a9.63 9.63 0 00-9.69 9.5v125.29a9.63 9.63 0 009.69 9.51h163a9.63 9.63 0 009.69-9.51V396.19a9.63 9.63 0 00-9.71-9.5zM244.32 461v34.66h-31.43v-33.84a22.62 22.62 0 01-9-17.94c0-12.74 10.87-23.06 24.27-23.06s24.27 10.32 24.27 23.06a22.49 22.49 0 01-8.11 17.12zM268 386.69h-79.7v-42.91a2.06 2.06 0 012.08-2h75.5a2.06 2.06 0 012.08 2z"
      stroke="#8fe5ff"
      strokeMiterlimit={10}
      strokeWidth={5}
      fill="url(#icons_nunet-17_svg__i)"
    />
  </svg>
);

export default SvgIconsnunet17;
