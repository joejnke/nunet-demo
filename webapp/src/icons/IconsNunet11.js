import React from "react";

const SvgIconsnunet11 = props => (
  <svg
    id="icons_nunet-11_svg__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 600 600"
    {...props}
  >
    <defs>
      <linearGradient
        id="icons_nunet-11_svg__linear-gradient"
        x1={538.12}
        y1={143.62}
        x2={283.37}
        y2={564.02}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.04} stopColor="#01a79e" />
        <stop offset={0.63} stopColor="#1c9fbf" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-11_svg__linear-gradient-2"
        x1={388.01}
        y1={52.66}
        x2={133.26}
        y2={473.05}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.62} stopColor="#00a79d" />
        <stop offset={0.82} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-11_svg__linear-gradient-3"
        x1={554.1}
        y1={153.31}
        x2={299.36}
        y2={573.7}
        xlinkHref="#icons_nunet-11_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-11_svg__linear-gradient-4"
        x1={495.18}
        y1={117.6}
        x2={240.44}
        y2={538}
        xlinkHref="#icons_nunet-11_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-11_svg__linear-gradient-5"
        x1={519.43}
        y1={132.29}
        x2={264.68}
        y2={552.69}
        xlinkHref="#icons_nunet-11_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-11_svg__linear-gradient-6"
        x1={344.83}
        y1={73.71}
        x2={373.69}
        y2={499.13}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-11_svg__linear-gradient-7"
        x1={449.84}
        y1={220.38}
        x2={445.15}
        y2={228.13}
        xlinkHref="#icons_nunet-11_svg__linear-gradient-6"
      />
      <linearGradient
        id="icons_nunet-11_svg__linear-gradient-8"
        x1={453.82}
        y1={267.96}
        x2={447.44}
        y2={278.49}
        xlinkHref="#icons_nunet-11_svg__linear-gradient-6"
      />
      <linearGradient
        id="icons_nunet-11_svg__linear-gradient-9"
        x1={450.33}
        y1={307.07}
        x2={446.11}
        y2={314.03}
        xlinkHref="#icons_nunet-11_svg__linear-gradient-6"
      />
      <style>{".icons_nunet-11_svg__cls-7{fill:#fff;opacity:.76}"}</style>
    </defs>
    <path
      d="M436.11 268.34V271s-16.7 48.1 17.21 41.28c21-4.22 18.18-21.68 13-34.37-9.94-10.59-22.47-10.61-30.21-9.57z"
      fill="url(#icons_nunet-11_svg__linear-gradient)"
    />
    <path
      d="M137.18 359.07l13.7-24.33s20.76-18.69 32.18-75.77 0-47.74 11.41-65.38 23.87-43.59 23.87-49.82 5.19-20.75 21.8-24.91c7.71-1.92 28.28-9.14 42.15-13.93 21.24-7.32 16.65 14.51 11.71 27.28-4.4 11.35-29.33 27.9-29.33 27.9L252.59 206l15.57 15.57h10.38l-.21 206.52s-15.33 21.85-50.65 19.77-86.42 39.22-86.42 39.22z"
      fill="url(#icons_nunet-11_svg__linear-gradient-2)"
    />
    <path
      d="M440.4 311.81V317s-7.29 24.57 3.53 27c20.28 4.55 21.4-22.91 19.4-27.27 0-.07-.09-.16-.13-.24-7.37-6.56-17.2-5.65-22.8-4.68z"
      fill="url(#icons_nunet-11_svg__linear-gradient-3)"
    />
    <path
      d="M427.17 170.89s39.34 15.91 45.57 27.27 0 17.05-9.34 22.73-33.12 7.69-36.23-1.41c-.86-2.49 0-13.64 0-13.64z"
      fill="url(#icons_nunet-11_svg__linear-gradient-4)"
    />
    <path
      d="M437.64 229.49V239c-4.89 17.83-9.44 26.14 8.21 34.09S466.6 264 466.6 264a193.35 193.35 0 002.18-22.44c.18-6.82-3.58-13-9.38-15.45a69.36 69.36 0 00-21.76-5.13z"
      fill="url(#icons_nunet-11_svg__linear-gradient-5)"
    />
    <rect
      x={278.33}
      y={142.94}
      width={162.07}
      height={290.04}
      rx={9.72}
      fill="url(#icons_nunet-11_svg__linear-gradient-6)"
    />
    <path
      className="icons_nunet-11_svg__cls-7"
      d="M295.86 172.07h127.7a5.39 5.39 0 015.39 5.39V393.9a4.39 4.39 0 01-4.39 4.39h-129.7a4.39 4.39 0 01-4.39-4.39V177.46a5.39 5.39 0 015.39-5.39z"
    />
    <ellipse
      className="icons_nunet-11_svg__cls-7"
      cx={359.71}
      cy={414.58}
      rx={11.48}
      ry={11.38}
    />
    <path
      fill="none"
      stroke="#c8e4f1"
      strokeLinecap="round"
      strokeMiterlimit={10}
      strokeWidth={2}
      d="M339.45 160.11h40.52"
    />
    <path
      d="M454.59 224.26a17.92 17.92 0 01-3.59.74 30 30 0 01-3.54.22A28.08 28.08 0 01444 225a17.24 17.24 0 01-3.55-.78 17.27 17.27 0 013.55-.79 26.33 26.33 0 013.55-.21 30 30 0 013.54.21 17.27 17.27 0 013.5.83z"
      fill="url(#icons_nunet-11_svg__linear-gradient-7)"
    />
    <path
      d="M460.86 273.23a34.83 34.83 0 01-5.12.78c-1.7.14-3.41.22-5.11.22s-3.41-.08-5.12-.22a33.42 33.42 0 01-5.11-.78 33.42 33.42 0 015.11-.78c1.71-.15 3.41-.23 5.12-.22s3.41.08 5.11.22a34.83 34.83 0 015.12.78z"
      fill="url(#icons_nunet-11_svg__linear-gradient-8)"
    />
    <path
      d="M456 311.6a21 21 0 01-4 .24 35.15 35.15 0 01-7.82-1 20.32 20.32 0 01-3.8-1.3 20.2 20.2 0 014-.25 36 36 0 017.82 1.05 20.42 20.42 0 013.8 1.26z"
      fill="url(#icons_nunet-11_svg__linear-gradient-9)"
    />
  </svg>
);

export default SvgIconsnunet11;
