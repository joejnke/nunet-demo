import React from "react";

const SvgIconsnunet21 = props => (
  <svg data-name="Layer 1" viewBox="0 0 600 600" {...props}>
    <defs>
      <linearGradient
        id="icons_nunet-21_svg__g"
        x1={-83.2}
        y1={415.9}
        x2={-84.01}
        y2={426.32}
        xlinkHref="#icons_nunet-21_svg__a"
      />
      <linearGradient
        id="icons_nunet-21_svg__b"
        x1={-265.33}
        y1={171.6}
        x2={168.13}
        y2={551.1}
        gradientTransform="matrix(1 0 0 -1 523.98 671.32)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.44} stopColor="#4ac2ba" />
        <stop offset={0.66} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-21_svg__c"
        x1={-78.67}
        y1={259.91}
        x2={-80.61}
        y2={284.67}
        gradientTransform="matrix(1 0 0 -1 523.98 546.36)"
        xlinkHref="#icons_nunet-21_svg__a"
      />
      <linearGradient
        id="icons_nunet-21_svg__d"
        x1={-114.89}
        y1={314.76}
        x2={-115.75}
        y2={325.87}
        gradientTransform="matrix(1 0 0 -1 595.26 641.42)"
        xlinkHref="#icons_nunet-21_svg__a"
      />
      <linearGradient
        id="icons_nunet-21_svg__e"
        x1={-78.67}
        y1={285.58}
        x2={-80.61}
        y2={310.34}
        gradientTransform="matrix(1 0 0 -1 523.98 597.71)"
        xlinkHref="#icons_nunet-21_svg__a"
      />
      <linearGradient
        id="icons_nunet-21_svg__f"
        x1={-114.89}
        y1={328.92}
        x2={-115.75}
        y2={340.04}
        gradientTransform="matrix(1 0 0 -1 595.26 669.76)"
        xlinkHref="#icons_nunet-21_svg__a"
      />
      <linearGradient
        id="icons_nunet-21_svg__a"
        x1={-0.05}
        y1={215.64}
        x2={-17.75}
        y2={441.94}
        gradientTransform="matrix(-1 0 0 1 364.56 0)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00927c" />
        <stop offset={0.65} stopColor="#1c8695" />
        <stop offset={1} stopColor="#2e7ea4" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-21_svg__h"
        x1={-114.89}
        y1={343.77}
        x2={-115.75}
        y2={354.89}
        gradientTransform="matrix(1 0 0 -1 595.26 699.46)"
        xlinkHref="#icons_nunet-21_svg__a"
      />
      <linearGradient
        id="icons_nunet-21_svg__i"
        x1={-114.89}
        y1={357.94}
        x2={-115.75}
        y2={369.05}
        gradientTransform="matrix(1 0 0 -1 595.26 727.79)"
        xlinkHref="#icons_nunet-21_svg__a"
      />
      <linearGradient
        id="icons_nunet-21_svg__j"
        x1={272.82}
        y1={163.11}
        x2={260.22}
        y2={449.09}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-21_svg__k"
        x1={216.94}
        y1={344.53}
        x2={314.16}
        y2={504.31}
        xlinkHref="#icons_nunet-21_svg__a"
      />
      <linearGradient
        id="icons_nunet-21_svg__l"
        x1={225.16}
        y1={402.62}
        x2={315.69}
        y2={486.11}
        xlinkHref="#icons_nunet-21_svg__b"
      />
    </defs>
    <path
      d="M396 226.13h102.24v219.06H396a5.67 5.67 0 01-5.67-5.67V231.79a5.67 5.67 0 015.67-5.67z"
      transform="rotate(-180 444.27 335.66)"
      fill="url(#icons_nunet-21_svg__b)"
    />
    <path
      d="M390.3 226.13l-31 18.13a8.69 8.69 0 00-4.3 7.49v158.56a8.67 8.67 0 003 6.52l32.35 28.36z"
      fill="url(#icons_nunet-21_svg__a)"
    />
    <path
      transform="rotate(-180 444.27 273.18)"
      fill="url(#icons_nunet-21_svg__c)"
      d="M399.96 264.66h88.61v17.04h-88.61z"
    />
    <path
      transform="rotate(-180 479.91 320.71)"
      fill="url(#icons_nunet-21_svg__d)"
      d="M471.25 316.01h17.33v9.4h-17.33z"
    />
    <path
      transform="rotate(-180 444.27 298.855)"
      fill="url(#icons_nunet-21_svg__e)"
      d="M399.96 290.34h88.61v17.04h-88.61z"
    />
    <path
      transform="rotate(-180 479.91 334.88)"
      fill="url(#icons_nunet-21_svg__f)"
      d="M471.25 330.18h17.33v9.4h-17.33z"
    />
    <circle
      cx={448.19}
      cy={421.49}
      r={5.03}
      fill="url(#icons_nunet-21_svg__g)"
    />
    <path
      transform="rotate(-180 479.91 349.73)"
      fill="url(#icons_nunet-21_svg__h)"
      d="M471.25 345.03h17.33v9.4h-17.33z"
    />
    <path
      transform="rotate(-180 479.91 363.895)"
      fill="url(#icons_nunet-21_svg__i)"
      d="M471.25 359.2h17.33v9.4h-17.33z"
    />
    <rect
      x={112.97}
      y={188.05}
      width={307.39}
      height={229.26}
      rx={9.92}
      fill="url(#icons_nunet-21_svg__j)"
    />
    <path
      d="M153.51 202.33h-8.59c-11 0-20 10.49-20 23.43v154c0 12.94 8.94 23.43 20 23.43H389c11 0 20-10.49 20-23.43v-154c0-12.94-8.94-23.43-20-23.43z"
      fill="#fff"
      opacity={0.76}
    />
    <path
      fill="url(#icons_nunet-21_svg__k)"
      d="M227.6 417.31h78.7v18.79h-78.7z"
    />
    <rect
      x={210.14}
      y={436.11}
      width={112.52}
      height={9.08}
      rx={0.47}
      fill="url(#icons_nunet-21_svg__l)"
    />
  </svg>
);

export default SvgIconsnunet21;
