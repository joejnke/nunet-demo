import React from "react";

const SvgIconsnunet08 = props => (
  <svg data-name="Layer 1" viewBox="0 0 600 600" {...props}>
    <defs>
      <linearGradient
        id="icons_nunet-08_svg__a"
        x1={29.55}
        y1={133.7}
        x2={886.9}
        y2={567.78}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#00a79d" />
        <stop offset={0.36} stopColor="#008e88" />
        <stop offset={0.74} stopColor="#007b78" />
        <stop offset={1} stopColor="#007472" />
      </linearGradient>
    </defs>
    <path
      d="M447.27 379.28c-1.24 0-2.46.07-3.68.17l-78-78a73 73 0 005.75-23.45h53.58a21.46 21.46 0 100-20.23h-54.83a73 73 0 00-6.95-19.42l75.24-75.25a42.86 42.86 0 10-32.16-29.45l-73.89 73.89a72.89 72.89 0 00-25.3-7.88v-50.5a21.46 21.46 0 10-20.23 0V200a72.85 72.85 0 00-17.61 5.08L195 130.8a42.91 42.91 0 10-28.6 33l69.6 69.7a72.84 72.84 0 00-10.19 26.78h-52a21.46 21.46 0 100 20.24h51.39a72.86 72.86 0 008 26.1l-71.57 71.56a42.88 42.88 0 1032.13 29.49L264 337.43a73 73 0 0025.29 7.88v54.89a21.46 21.46 0 1020.24 0V345a72.81 72.81 0 0027.59-10.33l70.66 70.66a42.91 42.91 0 1039.45-26zM256.43 274.67A43.57 43.57 0 11300 318.25a43.57 43.57 0 01-43.57-43.58z"
      fill="url(#icons_nunet-08_svg__a)"
    />
  </svg>
);

export default SvgIconsnunet08;
