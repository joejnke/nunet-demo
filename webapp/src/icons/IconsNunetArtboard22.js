import React from "react";

const SvgIconsnunetArtboard22 = props => (
  <svg
    id="icons_nunet_Artboard_2-2_svg__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 600 600"
    {...props}
  >
    <defs>
      <linearGradient
        id="icons_nunet_Artboard_2-2_svg__linear-gradient"
        x1={391.66}
        y1={51.19}
        x2={405.83}
        y2={473.55}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet_Artboard_2-2_svg__linear-gradient-2"
        x1={400.95}
        y1={290.54}
        x2={393.08}
        y2={438.21}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.67} stopColor="#1e9fc2" stopOpacity={0.87} />
        <stop offset={1} stopColor="#2e9ad6" stopOpacity={0.8} />
      </linearGradient>
      <linearGradient
        id="icons_nunet_Artboard_2-2_svg__linear-gradient-3"
        x1={239.02}
        y1={253.94}
        x2={296.79}
        y2={253.94}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.53} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet_Artboard_2-2_svg__linear-gradient-4"
        x1={189.52}
        y1={171.86}
        x2={269.98}
        y2={171.86}
        xlinkHref="#icons_nunet_Artboard_2-2_svg__linear-gradient-3"
      />
      <linearGradient
        id="icons_nunet_Artboard_2-2_svg__linear-gradient-5"
        x1={189.52}
        y1={328.83}
        x2={278.46}
        y2={328.83}
        xlinkHref="#icons_nunet_Artboard_2-2_svg__linear-gradient-3"
      />
      <linearGradient
        id="icons_nunet_Artboard_2-2_svg__linear-gradient-6"
        x1={192.73}
        y1={384.89}
        x2={191.44}
        y2={147.47}
        xlinkHref="#icons_nunet_Artboard_2-2_svg__linear-gradient-3"
      />
      <linearGradient
        id="icons_nunet_Artboard_2-2_svg__linear-gradient-7"
        x1={308.2}
        y1={-4.52}
        x2={322.39}
        y2={418.5}
        xlinkHref="#icons_nunet_Artboard_2-2_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet_Artboard_2-2_svg__linear-gradient-8"
        x1={294.14}
        y1={79.73}
        x2={308.33}
        y2={502.79}
        xlinkHref="#icons_nunet_Artboard_2-2_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet_Artboard_2-2_svg__linear-gradient-9"
        x1={176.49}
        y1={252.96}
        x2={207.55}
        y2={252.96}
        xlinkHref="#icons_nunet_Artboard_2-2_svg__linear-gradient-3"
      />
      <style>
        {
          ".icons_nunet_Artboard_2-2_svg__cls-10,.icons_nunet_Artboard_2-2_svg__cls-2{fill:#fff}.icons_nunet_Artboard_2-2_svg__cls-2{opacity:.76}.icons_nunet_Artboard_2-2_svg__cls-10{stroke-miterlimit:10;stroke:#2e9ad6}"
        }
      </style>
    </defs>
    <rect
      x={292.92}
      y={172.85}
      width={210.59}
      height={147.57}
      rx={9.92}
      fill="url(#icons_nunet_Artboard_2-2_svg__linear-gradient)"
    />
    <path
      className="icons_nunet_Artboard_2-2_svg__cls-2"
      d="M320.7 182h-5.88c-7.56 0-13.69 6.75-13.69 15.08v99.11c0 8.33 6.13 15.08 13.69 15.08H482c7.56 0 13.69-6.75 13.69-15.08v-99.07c0-8.33-6.13-15.08-13.69-15.08z"
    />
    <rect
      x={286.63}
      y={319.9}
      width={224.55}
      height={17.81}
      rx={8.4}
      fill="url(#icons_nunet_Artboard_2-2_svg__linear-gradient-2)"
    />
    <ellipse
      className="icons_nunet_Artboard_2-2_svg__cls-2"
      cx={398.22}
      cy={328.81}
      rx={5.82}
      ry={6.03}
    />
    <path
      stroke="url(#icons_nunet_Artboard_2-2_svg__linear-gradient-3)"
      strokeWidth={5}
      strokeMiterlimit={10}
      fill="none"
      d="M239.02 253.94h57.77"
    />
    <path
      stroke="url(#icons_nunet_Artboard_2-2_svg__linear-gradient-4)"
      strokeWidth={5}
      strokeMiterlimit={10}
      fill="none"
      d="M269.98 159.38h-77.96v27.47"
    />
    <path
      stroke="url(#icons_nunet_Artboard_2-2_svg__linear-gradient-5)"
      strokeWidth={5}
      strokeMiterlimit={10}
      fill="none"
      d="M192.02 316.47v-14.55 51.32h86.44"
    />
    <path
      d="M271.35 263.42v-19.69h-23.64a55.89 55.89 0 00-9.33-22.63l16.68-16.69-13.95-13.91-16.67 16.68a56.44 56.44 0 00-22.58-9.37v-23.6h-19.69v23.6a56.24 56.24 0 00-22.62 9.37l-16.67-16.68L129 204.41l16.68 16.69a56.23 56.23 0 00-9.37 22.63h-23.6v19.69h23.6a56 56 0 009.34 22.58L129 302.74l13.91 13.91L159.55 300a56.2 56.2 0 0022.62 9.34v23.64h19.69V309.3a56.2 56.2 0 0022.58-9.3l16.67 16.69 13.95-13.91L238.38 286a55.66 55.66 0 009.33-22.59zM192 289.81a36.24 36.24 0 1136.23-36.24A36.24 36.24 0 01192 289.81z"
      fill="url(#icons_nunet_Artboard_2-2_svg__linear-gradient-6)"
    />
    <rect
      x={266.56}
      y={281.12}
      width={107}
      height={135.82}
      rx={20.22}
      fill="url(#icons_nunet_Artboard_2-2_svg__linear-gradient-7)"
    />
    <ellipse
      className="icons_nunet_Artboard_2-2_svg__cls-2"
      cx={320.06}
      cy={404.87}
      rx={6.63}
      ry={7.28}
    />
    <path
      className="icons_nunet_Artboard_2-2_svg__cls-2"
      d="M284 295.31h71.35a7.91 7.91 0 017.91 7.91v86.21a4.91 4.91 0 01-4.91 4.91H281a4.91 4.91 0 01-4.91-4.91v-86.21a7.91 7.91 0 017.91-7.91z"
    />
    <path
      stroke="#c8e4f1"
      strokeMiterlimit={10}
      fill="none"
      d="M312.57 288.73h12.27"
    />
    <circle
      className="icons_nunet_Artboard_2-2_svg__cls-10"
      cx={296.79}
      cy={252.96}
      r={4.41}
    />
    <rect
      x={266.53}
      y={104.78}
      width={60.54}
      height={108.34}
      rx={4.85}
      fill="url(#icons_nunet_Artboard_2-2_svg__linear-gradient-8)"
    />
    <path
      className="icons_nunet_Artboard_2-2_svg__cls-2"
      d="M276.45 115.66h40.95a5.39 5.39 0 015.39 5.39v74.72a4.39 4.39 0 01-4.39 4.39h-42.95a4.39 4.39 0 01-4.39-4.39V121a5.39 5.39 0 015.39-5.34z"
    />
    <ellipse
      className="icons_nunet_Artboard_2-2_svg__cls-2"
      cx={296.92}
      cy={206.24}
      rx={4.29}
      ry={4.25}
    />
    <path
      strokeLinecap="round"
      stroke="#c8e4f1"
      strokeMiterlimit={10}
      fill="none"
      d="M287.97 111.19h15.14"
    />
    <circle
      className="icons_nunet_Artboard_2-2_svg__cls-10"
      cx={269.98}
      cy={353.23}
      r={4.41}
    />
    <circle
      className="icons_nunet_Artboard_2-2_svg__cls-10"
      cx={269.98}
      cy={159.38}
      r={4.41}
    />
    <circle
      cx={192.02}
      cy={252.96}
      r={15.53}
      fill="url(#icons_nunet_Artboard_2-2_svg__linear-gradient-9)"
    />
  </svg>
);

export default SvgIconsnunetArtboard22;
