# Nunet Demo Web App

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### Setup envoy proxy to use grpc-web

In order to call grpc client from browser, you have to setup a proxy server that can map your browser request to grpc call and vice versa. Currently, envoy proxy is the best option to achieve this; and using docker, setup the required envoy proxy using the commands below inside the project folder:

-   `docker build -t grpc_envoy -f ./envoy.Dockerfile .`
-   `docker run --net=host grpc_envoy`

### Make sure grpc server is running

You have to make sure the corresponding grpc server is running. You can find this grpc server from:
[https://gitlab.com/nunet/nunet-demo/](https://gitlab.com/nunet/nunet-demo/)

### How to generate necessary client side grpc code from session.proto file (Optional if your system is ubuntu 18 or 19)

You must install `protoc` on your system and its corresponding `protoc-gen-grpc-web` protoc plugin. For more information see the official grpc-web code-generator section on their README [https://github.com/grpc/grpc-web#code-generator-plugin](here).

Use the below command inside `src/grpc/` folder of the project:
`protoc -I=. session.proto --js_out=import_style=commonjs:. --grpc-web_out=import_style=commonjs,mode=grpcwebtext:.`

This command will generate two javascript files;`session_grpc_web_pb.js` and `session_bp.js`. Note if the project failed to compile after the above command, enter `/* eslint-disable */` comment in the start of the above files to disable eslint in them.
