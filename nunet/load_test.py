#Saves successfull and failed calls by accepting number of request from the user and runs all concurently
import logging

import organizer

import time

import sys

import docker

import base64

import random

import json

import ast

import requests

import os 

from multiprocessing import Process,current_process

import multiprocessing as mp

import datetime

class load_test():
    def __init__(self):
        self.yolo_arg = ["python3", "-m", "service.object_detection_service", "--grpc-port","8889"]
        self.yolo_img="yolov3-object-detection"
        self.cntk_arg = ["/root/anaconda3/envs/cntk-py35/bin/python", "-m", "service.image_recon_service", "--grpc-port","8890"]
        self.cntk_img="cntk-image-recon"
        # self.img = base64.b64encode(requests.get("https://singnet.github.io/dnn-model-services/assets/users_guide/bulldog.jpg").content)
      
        try:
            with open("bulldog.jpg", "rb") as image_file:
             encoded_string = base64.b64encode(image_file.read())
             self.img = encoded_string
        except:
            self.img = base64.b64encode(requests.get("https://singnet.github.io/dnn-model-services/assets/users_guide/bulldog.jpg").content)
        self.cntk_status=False
        self.yolo_status=False
    
    def create_net(self):
        orch = organizer.Orchestrator()
        call=orch.call(self.img, "test")
        try:
            net=orch.client.networks.get("nunet")
        except:
            net=orch.client.networks.create("nunet")       
      
        return net

    def success_rate(self,net,output):
        orch = organizer.Orchestrator()

        if not os.path.exists('log'):
                os.mkdir('log')

        call=orch.call(self.img, "test")
        for tag, lg , breed in call:
            if 'Bulldog' in str(breed):
                self.cntk_status=True
            if 'Dog is detected in the picture' in str(lg):
                self.yolo_status=True

        if self.yolo_status==True and self.cntk_status==True:
            output.put([1,1])
        elif self.yolo_status==True and self.cntk_status==False:
            output.put([1,0])

        else:
            output.put([0,0])
        
    def run_test(self,requests):
        output = mp.Queue()
        net=self.create_net()
        start_time = time.time()
        processes=[]
        with open("quit.txt","w") as f:
            f.write("n")
        n = requests
        processes = [Process(target=self.success_rate,args=(net,output)) for x in range(int(n))]
        for p in processes:
            p.start()
        # for p in processes:
        #     p.join()
        while (True):
            print("enter y in quit.txt to exit")
            time.sleep(3)
            with open("quit.txt","r") as f:
                if "y" in f.readline():
                    break
        
        end_time = time.time()
        results = []
        for p in processes:
            try:
                results.append(output.get(timeout=1.5))
            except:
                print("error fetching element from queue")
        now = datetime.datetime.now()
        path="load_test/"
        try:
            os.mkdir(path)
        except:          
            pass
        success=0
        failed=0
        cntk_failed=0
        yolo_failed=0
        for result in results:
            try:
                if result==[1,1]:
                    success+=1
                elif result[1,0]:
                    failed+=1
                    cntk_failed+=1
                else:
                    failed+=1
                    yolo_failed+=1
            except:
                failed+=1
                cntk_failed+=1
                yolo_failed+=1

        
        
        secs_taken = str(((end_time - start_time) ))
        mins_taken = str(((end_time - start_time) / 60))
        
        print('Testing took ' + secs_taken + ' seconds.')
        print('Testing took ' + mins_taken + ' minutes.')

        res = {"success": success,"failed": failed,"cntk failed": cntk_failed,"yolo failed": yolo_failed,"testing took in secs":float(secs_taken),"testing took in mins":mins_taken}        
        with open(path+'/'+str(now)+'.json', 'w') as json_file:
             json.dump(res, json_file)
        
        with open(path+'/'+str(now)+'.json', 'r') as reader:
                contents=reader.read()
        json_data=json.loads(contents)
        
        print("success",json_data['success'])
        print("failed",json_data['failed'])
        print("cntk failed",json_data['cntk failed'])
        print("yolo failed",json_data['yolo failed'])


if __name__ == '__main__':
    orch = organizer.Orchestrator()
   # load_test=load_test()
   # load_test.run_test()
