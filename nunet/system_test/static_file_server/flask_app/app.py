import os
from os import listdir
from os.path import expanduser
from flask import Flask, render_template

web_dir = '/home/system-test/uploads'
template_dir = '/home/system-test/reports_server/nunet-demo/nunet/system_test/static_file_server/flask_app/templates'

# os.chdir(web_dir)

# app = Flask(__name__)
# app = Flask(__name__, template_folder=template_dir, static_url_path=web_dir)
# app = Flask(__name__, template_folder=template_dir, root_path=web_dir)
app = Flask(__name__, template_folder=template_dir, static_folder=web_dir, static_url_path='/allure_reports')


@app.route('/')
def static_file_dir_tree():
    path = web_dir
    # path = expanduser(web_dir)
    # path = expanduser(u'/home/system-test/system_test/static_file_server/flask_app/static')
    dir_list = list()
    try:
        dir_list = sorted(listdir(path), reverse=True)
    except OSError:
        pass  # handle errors due to empty list of html directories
    # print (dir_list)
    return render_template('index.html', html_report_dir_list=dir_list)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=9999, debug=False)
