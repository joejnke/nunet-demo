behave -f allure_behave.formatter:AllureFormatter -o reports/ ./features
./allure-2.13.0/bin/allure generate reports/ -o html_reports/
python3 report.py
#coverage run --source='.' -m behave
#codecov --commit $GIT_COMMIT_SHA -t $CODECOV_TOKEN