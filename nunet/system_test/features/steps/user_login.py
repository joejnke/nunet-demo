from behave import *
import unittest
tc = unittest.TestCase('__init__')
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import os
import logging

try:
    if os.environ['SYSTEM_TEST_ADDRESS'] == 'true':
        SYSTEM_TEST_NUNET_ADDRESS = "https://demo.nunet.io:3031/login"
        print('Using system test nunet instance ...')
    else:
        SYSTEM_TEST_NUNET_ADDRESS = "http://127.0.0.1:3000/login"
        print('Using local nunet instance ...')
except:
    logging.exception('message')
    SYSTEM_TEST_NUNET_ADDRESS = "http://127.0.0.1:3000/login"
    print('Using local nunet instance ...')


delay = 60 # seconds
delay_wait_for_container_deletion = 60



@given(u'user visits login page')
def step_impl(context):
    for browser in context.browser:
        browser.get(SYSTEM_TEST_NUNET_ADDRESS)

@when(u'puts in login details')
def step_impl(context):
    for browser in context.browser:
        elem = browser.find_element_by_name("email")
        elem.send_keys("test@test.com")
        elem = browser.find_element_by_name("password")
        elem.send_keys("test")
        # browser.find_element_by_xpath("//button[@type='button']").click()
        browser.find_element_by_xpath("//*[contains(text(), 'Sign In')]").click()

@then(u'sees consumer perspecitve page')
def step_impl(context):
    for browser in context.browser:
        # WebDriverWait(browser, delay).until(EC.presence_of_all_elements_located((By.XPATH,"//h6")))
        WebDriverWait(browser, delay).until(EC.presence_of_all_elements_located((By.XPATH,"//*[contains(text(), 'Consumer Perspective')]")))
        tc.assertIn("Consumer Perspective", browser.page_source)