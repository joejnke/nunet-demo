import os
import datetime
import shutil


# Put this into python script
# mkdir "${HOME}/.ssh"
# echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ${HOME}/.ssh/config
# eval $(ssh-agent -s)
# echo "${SYSTEM_TEST_PUBLIC_KEY}" > "${HOME}/.ssh/id_rsa.pub"
# echo "${SYSTEM_TEST_PRIVATE_KEY}" > "${HOME}/.ssh/id_rsa"
# chmod 700 "${HOME}/.ssh/id_rsa"

cmds = ['mkdir -p \"${HOME}/.ssh\"',
        'echo \"Host *\n\tStrictHostKeyChecking no\n\n\" > ${HOME}/.ssh/config',
        'eval $(ssh-agent -s)',
        'echo \"${SYSTEM_TEST_PUBLIC_KEY}\" > \"${HOME}/.ssh/id_rsa.pub\"',
        'echo \"${SYSTEM_TEST_PRIVATE_KEY}\" > \"${HOME}/.ssh/id_rsa\"',
        'chmod 700 \"${HOME}/.ssh/id_rsa\"']


for cmd in cmds:
    os.system(cmd)


new_name = 'html_reports_' + str(datetime.datetime.now()).replace(' ','~').replace(':','_')
shutil.copytree('html_reports',new_name)
shutil.rmtree('html_reports')

rsync = "rsync -avz -e 'ssh'" + ' /nunet-demo/system_test/'+ new_name + ' system-test@195.201.197.25:/home/system-test/uploads'
os.system(rsync)