import logging
import grpc
import session_manager_client as sm
from datetime import datetime
import json
import os

class Session:

    def __init__(self):
        self.stub = sm.get_stub()
        self.email = "test2@test.com"
        self.password="test2@test.com"
        self.device_name="samsung"
        self.token=""
        self.img=["artifacts/image1/img1.jpg","artifacts/image1/img2.jpg","artifacts/image1/img3.jpg","artifacts/image1/img4.jpg","artifacts/image1/img5.jpg","artifacts/image1/img6.jpg","artifacts/image1/img7.jpg","artifacts/image1/img8.jpg","artifacts/image1/img9.jpg","artifacts/image1/img10.jpg"]

    def signup(self):
        try:
             sm.signup(self.stub, self.email, self.password)
        except:
             logging.info("You have already registered")

    def login(self):
        tkn=sm.login(self.stub, self.email, self.password,self.device_name)
        self.token=tkn

    def execute(self):
        yolo,cntk=sm.Execute(self.stub, self.token, "bulldog.jpg")
        print(yolo)
        print(cntk)
    
    def execute_resource_usage(self):
        average_yolo=[0,0,0,0,0,0,0]
        average_cntk=[0,0,0,0,0,0,0]
        sd_yolo=[0,0,0,0,0,0,0]
        sd_cntk=[0,0,0,0,0,0,0]
        values=['memory_usage','cpu_usage','total_memory','time_taken','net_tx','net_rx']
        open("resource_usage.txt", 'w')
        conts=["yolov","cntk"]
        tags=["max_memory","total_cpu","total_memory","time_taken","net_tx","net_rx"]
        #0:mem_usage,#1:cpu,#2total_mem,#3time_taken,#4nettx,#5netrx
        xi_yolo=[]
        xi_cntk=[]
        image_sizes=[]

        for i in range(10):
            print("execution:",str(i))
            image_sizes.append(os.stat(self.img[i]).st_size)
            yolo,cntk=sm.Execute(self.stub, self.token, self.img[i])
            xi_yolo.append(json.loads(yolo))
            xi_cntk.append(json.loads(cntk))
            for j in range(6):
                average_yolo[j]+=json.loads(yolo)[values[j]]
                average_cntk[j]+=json.loads(cntk)[values[j]]
        #mean
        for i in range(len(average_cntk)):
            average_yolo[i]/=len(xi_yolo)
            average_cntk[i]/=len(xi_cntk)
        i=0

        with open("resource_usage.txt", 'a') as f:
            f.write("[\n")

        for x_yolo in xi_yolo:
            for j in range(6):
                sd_yolo[j]+=((x_yolo[values[j]]-average_yolo[j])**2)

            image_size=str(image_sizes[i])
            ind=str(i)+"_yolo"
            with open("resource_usage.txt", 'a') as f:             
                if i==0:
                    f.write("{"+'"process_name":"'+ind+'",'+'"image_size":'+image_size+','+'"max_memory":'+str(x_yolo['memory_usage'])+','+'"total_cpu":'+str(x_yolo['cpu_usage'])+','+'"total_memory":'+str(x_yolo['total_memory'])+','+'"time_taken":'+str(x_yolo['time_taken'])+','+'"net_tx":'+str(x_yolo['net_tx'])+','+'"net_rx":'+str(x_yolo['net_rx'])+'},\n')
                else:
                    f.write("{"+'"process_name":"'+ind+'",'+'"image_size":'+image_size+','+'"max_memory":'+str(x_yolo['memory_usage'])+','+'"total_cpu":'+str(x_yolo['cpu_usage'])+','+'"total_memory":'+str(x_yolo['total_memory'])+','+'"time_taken":'+str(x_yolo['time_taken'])+','+'"net_tx":'+str(x_yolo['net_tx'])+','+'"net_rx":'+str(x_yolo['net_rx'])+'},\n')
            i+=1
        for i in range(len(sd_yolo)):
            sd_yolo[i]=(sd_yolo[i]/len(xi_yolo))**0.5
        i=0
        for x_cntk in xi_cntk:
            for j in range(6):
                sd_cntk[j]+=((x_cntk[values[j]]-average_cntk[j])**2)
            ind=str(i)+"_cntk"
            image_size=str(image_sizes[i])
            with open("resource_usage.txt", 'a') as f:             
                f.write("{"+'"process_name":"'+ind+'",'+'"image_size":'+image_size+','+'"max_memory":'+str(x_cntk['memory_usage'])+','+'"total_cpu":'+str(x_cntk['cpu_usage'])+','+'"total_memory":'+str(x_cntk['total_memory'])+','+'"time_taken":'+str(x_cntk['time_taken'])+','+'"net_tx":'+str(x_cntk['net_tx'])+','+'"net_rx":'+str(x_cntk['net_rx'])+'},\n')
            i+=1       
        for i in range(len(sd_cntk)):
            sd_cntk[i]=(sd_cntk[i]/len(xi_cntk))**0.5
        
        for cont in conts:
            for i in range(len(tags)):
                if cont=="yolov":
                    print("average:"+str(cont)+"_"+str(tags[i])+":"+str(average_yolo[i]))
                    print("standard deviation:"+str(cont)+"_"+str(tags[i])+":"+str(sd_yolo[i]))
                else:
                    print("average"+str(cont)+"_"+str(tags[i])+":"+str(average_cntk[i]))
                    print("standard deviation"+str(cont)+"_"+str(tags[i])+":"+str(sd_cntk[i]))
        
        with open("resource_usage.txt", 'a') as f:
            f.write("{"+'"average_yolo"'+':{'+'"max_memory":'+str(average_yolo[0])+','+'"total_cpu":'+str(average_yolo[1])+','+'"total_memory":'+str(average_yolo[2])+','+'"time_taken":'+str(average_yolo[3])+','+'"net_tx":'+str(average_yolo[4])+','+'"net_rx":'+str(average_yolo[5])+'},\n')                
            f.write('"average_cntk"'+':{'+'"max_memory":'+str(average_cntk[0])+','+'"total_cpu":'+str(average_cntk[1])+','+'"total_memory":'+str(average_cntk[2])+','+'"time_taken":'+str(average_cntk[3])+','+'"net_tx":'+str(average_cntk[4])+','+'"net_rx":'+str(average_cntk[5])+'},\n')
            f.write('"standard_deviation_yolo"'+':{'+'"max_memory":'+str(sd_yolo[0])+','+'"total_cpu":'+str(sd_yolo[1])+','+'"total_memory":'+str(sd_yolo[2])+','+'"time_taken":'+str(sd_yolo[3])+','+'"net_tx":'+str(sd_yolo[4])+','+'"net_rx":'+str(sd_yolo[5])+'},\n')                
            f.write('"standard_deviation_cntk"'+':{'+'"max_memory":'+str(sd_cntk[0])+','+'"total_cpu":'+str(sd_cntk[1])+','+'"total_memory":'+str(sd_cntk[2])+','+'"time_taken":'+str(sd_cntk[3])+','+'"net_tx":'+str(sd_cntk[4])+','+'"net_rx":'+str(sd_cntk[5])+'}}\n]')
  
        with open("resource_usage.txt", 'r') as f:
           print(f.read())
           
    def logout(self):
        try:
           sm.logout(self.stub,self.device_name,self.password)
        except:
            logging.info("You have already logged out")

if __name__ == '__main__':
   sess=Session()
   sess.signup()
   sess.login()
   sess.execute_resource_usage()
   sess.logout()