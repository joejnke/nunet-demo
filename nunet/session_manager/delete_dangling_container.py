import time
import datetime
import os
from multiprocessing import Process
import logging
import docker

class RemoveContainer:
    def __init__(self):
        self.sleep_time=3600.0
        self.client=docker.from_env()
    def remove(self):
        print("running ...")
        while (True):
            time.sleep(self.sleep_time)
            for yolo_cont in self.client.containers.list(all=True, filters={"ancestor":"yolov3-object-detection"}):
                log=yolo_cont.logs(until=int(time.time()-self.sleep_time))
                time.sleep(20.0)
                if "b''"!=str(log) or yolo_cont.status == "created":
                    yolo_cont.remove(force=True)
                    print("dangling yolov-cont removed")

            for cntk_cont in self.client.containers.list(all=True, filters={"ancestor":"cntk-image-recon"}):
                log=cntk_cont.logs(until=int(time.time()-self.sleep_time))
                time.sleep(20.0)
                if "b''"!=str(log) or cntk_cont.status == "created":
                    cntk_cont.remove(force=True)
                    print("dangling cntk-cont removed")

    def start_remove(self):
            Process(target=self.remove,args=()).start()
    
if __name__ == "__main__":
    rc= RemoveContainer()
    rc.start_remove()