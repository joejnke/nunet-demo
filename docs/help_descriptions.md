# NuNet Platform Demo Application 

<!-- MarkdownTOC autolink="true" -->

- [General description](#general-description)
- [Views](#views)
	- [Home](#home)
	- [Execute Task](#execute-task)
	- [Results page](#results-page)
	- [Reward Table](#reward-table)
	- [Consumer Perspective](#consumer-perspective)
	- [Task history](#task-history)
	- [Provider Perspective](#provider-perspective)
- [Telemetry](#telemetry)
	- [Total CPU](#total-cpu)
	- [RAM](#ram)
	- [MaxRAM](#maxram)
	- [Time](#time)
	- [NET](#net)
	- [NetTime](#nettime)

<!-- /MarkdownTOC -->


## General description

The NuNet Demo Application is designed to demonstrate the NuNet platform -- a global economy of decentralized computing. The demonstration utilizes a simple use-case of dog breed recognition game, where users submit an image of a dog (via mobile phone camera or from disk) and initiate a pipeline of AI services provided by SingularityNET, which subsequently recognize an object and a dog breed.

The demo implements the proof-of-concept of:
* execution of business logic which uses multiple SingularityNET AI services connected in ad-hoc task-dependent network;
* the usage of independent hardware environments for decentralized execution of computation processes in independent hardware environments and physical machines;
* orchestration of the decentralized execution and data exchanges between computation processes running in independent execution environments;
* telemetry of resource usage in decentralized network (see [Telemetry](Telemetry));
* economy of computational processes, including estimation of execution costs based on independent resource pricing of providers of execution environments, usage profiling and orchestration of token payments to hardware providers.

The live NuNet platform activity can be observed via Network Statistics page at https://stats.nunet.io;

## Views

### Home

The home screen shortly introduces the logic of an application and the dog breed recognition game. Home screen is displayed only for when a user logins for the first time, otherwise a user is directed immediately to 'Execute Task' page. Each new user is awarded an initial amount of NTXd tokens for covering the costs of decentralized hardware usage on the platform. Successful tasks allow a user to earn more tokens and execute more tasks.

### Execute Task

Execute Task view exposes the main functionality of the application by:

(1) allowing users to submit an image of the dog;
(2) showing the progress, process statistics and telemetry information (see [Telemetry](Telemetry)) of the object detection process (separate [SingularityNET service](https://beta.singularitynet.io/servicedetails/org/snet/service/yolov3-object-detection) running in isolated environment);
(3) showing the progress, process statistics and telemetry information of the dog breed detection process (separate [SingularityNET service](https://beta.singularitynet.io/servicedetails/org/snet/service/cntk-image-recon) running in isolated environment);

The application automatically steps through these stages and presents results of each of them on the screen as the corresponding processes are carried out by decentralized execution environments and hardware devices within the platform. After processes finish, a user can view overall results on the separate page;

### Results page

The Results page provides task execution summary by displaying the initial picture with the highlighted object box and the name of detected breed with highest confidence as judged by the algorithm. A user can also check the raw JSON output of the dog breed detection algorithm which provides all candidate dog breeds with their probability estimations. A user is able to share the result screen via his/her social media accounts (Twitter, Telegram, LinkedIn and Facebook).

The summarized telemetry information of computational resources used by the NuNet Platform for the execution of a a task task is displayed under the title 'Resources'. 

The tokenomic aspect of the executed task is provided under the title 'Tokens'. This includes reward received for completing the task, cost of used computing resources and the earnings balance in NTXd tokens.

### Reward Table

The Reward Table page displays the list of all detectable dog breeds, their sample images and corresponding bounty rewards in NTXd.

### Consumer Perspective

In NuNet, consumers are users of the platform which use it primarily for defining and executing AI tasks. This view summarizes consumer's activity in terms of the number of submitted and successful tasks, balance of NTXd tokens in consumer's account, amounts of NTXd paid to hardware providers and earned from successful tasks. For registered users, the view displays balance of all historic activity, for guest users -- only for tasks and processes executed during a single session.

On the fully developed NuNet platform, consumers will be able to earn tokens by providing combinations of available AI services and hardware resources to other consumers which are in need of specific business logic. The present demo application shows a simple case of what will grow into the global economy of decentralized computing processes.

### Task history

This view allows consumers to access the list of all executed tasks and their result pages. For registered users, the history of activity in the platform is stored in internal database and can be accessed anytime. Guest users' activity is flushed on logout or browser close and is therefore lost.

### Provider Perspective

The computational capacity of NuNet's platform is composed from computational capacities of decentralized hardware devices, owned and registered to the platform by independent providers. The API of APIs of NuNet will provide a way for hardware providers to register their computational capacities and set their usage price in NTXd and for consumers to bid for those resources when executing computational tasks.

Provider perspective view allows for hardware providers to access the full history of the usage of their devices, including computational processes completed on each registered device and NTXd tokens earned by them. 

Further, providers can inspect each device separately and access:

* limits of computing resources dedicated to the usage on NuNet platform by each device (in terms of their CPU, RAM, Network time and system time);
* historic usage of a device in the platform in terms of used computing resources;
* price of computing resources for each device.

Fully developed NuNet platform will allow providers to fully manage their pool of hardware devices: register new devices, set and change their computing resource prices manually or via a learning algorithms, etc.

## Telemetry

### Total CPU 

The metric of processor work used to complete a computational process. It is measured in **MTicks** (million ticks) which shows how many processor cycles were used during the execution of the process. It is roughly equivalent to the number of executed instructions.

### RAM 

The metric of memory used by a computational process. It is measured in **MBs** (megabyte seconds) which is calculated by adding spot RAM usages sampled every second for the  whole Time of execution (see metric *Time*). 

### MaxRAM

The largest RAM utilization in **MB** that occurred during execution of a computational process. It indicates a minimum RAM requirement of an execution environment running the process.

### Time

The wall time of the process execution measured in **seconds**.

### NET

The sum of data transferred and received via network interfaces during process execution, in **KB** (kilobytes);

### NetTime

The average amount data transferred and received via network interfaces during process execution, in **KB/s** (kilobytes per second);