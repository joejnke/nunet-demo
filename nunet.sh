if [[ "$1" == "production" ]]; then
    if [[ "$2" == "up" ]]; then
        docker-compose -f docker-compose.all.yml up --force-recreate --build production_proxy production_nunet production_webapp
    elif [[ "$2" == "build" ]]; then
        docker-compose -f docker-compose.all.yml build production_proxy production_nunet production_webapp
    elif [[ "$2" == "stop" ]]; then
        docker-compose -f docker-compose.all.yml stop production_proxy production_nunet production_webapp
    else
        echo "command production can only be used with up, build or stop"
    fi
elif [[ "$1" == "dev" ]]; then
    if [[ "$2" == "up" ]]; then
        docker-compose -f docker-compose.all.yml up --force-recreate --build dev_proxy dev_nunet dev_webapp
    elif [[ "$2" == "build" ]]; then
        docker-compose -f docker-compose.all.yml build dev_proxy dev_nunet dev_webapp
    elif [[ "$2" == "stop" ]]; then
        docker-compose -f docker-compose.all.yml stop dev_proxy dev_nunet dev_webapp
    else
        echo "command dev can only be used with up, build or stop"
    fi
elif [[ "$1" == "test" ]]; then
    if [[ "$2" == "up" ]]; then
        docker rm -f test_proxy test_nunet test_webapp  # remove containers to stop reusing database
        docker-compose -f docker-compose.all.yml up --force-recreate --build test_proxy test_nunet test_webapp
    elif [[ "$2" == "build" ]]; then
        docker-compose -f docker-compose.all.yml build test_proxy test_nunet test_webapp
    elif [[ "$2" == "stop" ]]; then
        docker-compose -f docker-compose.all.yml stop test_proxy test_nunet test_webapp
    else
        echo "command test can only be used with up, build or stop"
    fi
elif [[ "$1" == "local" ]]; then
    if [[ "$2" == "up" ]]; then
        docker-compose -f docker-compose.all.yml up --force-recreate --build local_proxy local_nunet local_webapp
    elif [[ "$2" == "build" ]]; then
        docker-compose -f docker-compose.all.yml build local_proxy local_nunet local_webapp
    elif [[ "$2" == "stop" ]]; then
        docker-compose -f docker-compose.all.yml stop local_proxy local_nunet local_webapp
    else
        echo "command local can only be used with up, build or stop"
    fi
elif [[ "$1" == "localnp" ]]; then
    if [[ "$2" == "up" ]]; then
        docker rm -f local_proxy local_nunet_no_persist local_webapp
        docker-compose -f docker-compose.all.yml up --force-recreate --build local_proxy local_nunet_no_persist local_webapp
    elif [[ "$2" == "build" ]]; then
        docker-compose -f docker-compose.all.yml build local_proxy local_nunet_no_persist local_webapp
    elif [[ "$2" == "stop" ]]; then
        docker-compose -f docker-compose.all.yml stop local_proxy local_nunet_no_persist local_webapp
    else
        echo "command test can only be used with up, build or stop"
    fi
else
    echo "available commands: production, dev, test, local, localnp"
fi
